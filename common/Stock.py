import string, random

def Random_text():
    """随机生成字符串"""
    str = (chr(random.randint(0x4e00, 0x9fbf))+chr(random.randint(0x4e00, 0x9fbf)))*2
    return str


def Random_EN(num):
    str_en = string.ascii_letters
    str_EN = random.sample(str_en,num)
    return str_EN


def Random_phone():
    """随机生成电话号码"""
    prelist = ["130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "147", "150", "151", "152",
               "153", "155", "156", "157", "158", "159", "186", "187", "188", "191"]
    return random.choice(prelist) + "".join(random.choice("0123456789") for i in range(8))


if __name__ == '__main__':
    Random_phone()
    Random_text()
    Random_EN(10)
    