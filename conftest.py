"""设置前置操作"""
from common.logger import Log


"""Code description:配置信息"""
import pytest, allure, xlrd
from selenium import webdriver

"""
session：是多个文件调用一次，可以跨.py文件调用，每个.py文件就是module
module：每一个.py文件调用一次，该文件内又有多个function和class
class：每一个类调用一次，一个类中可以有多个方法
function：每一个函数或方法都会调用
"""
driver = None


@pytest.fixture(scope="module", autouse=True)
def open_browser_login(browser='chrome'):  # 不输入参数,默认打开谷歌浏览器
    """
    通过浏览器名称,打开对应的浏览器
    :param browser: 浏览器名称
    :param kwargs: 浏览器参数
    :return: driver,即webdriver.browser()
    """
    global driver
    if driver is None:
        driver = webdriver.Chrome()
        driver.get('https://music.163.com/')
        workbook_open = xlrd.open_workbook('./data/blogs_cookies.xls')  # 使用xlrd创建一个工作薄对象
        sheet = workbook_open.sheet_by_name('sheet1')  # 根据工作表的名称创建表格对象
        cookie_list = []  # 创建一个空列表
        for row_num in range(1, sheet.nrows):
            cookie_dict = {}  # 创建一个空字典
            cookie_dict['name'] = sheet.cell_value(row_num, 0)
            cookie_dict['value'] = sheet.cell_value(row_num, 1)
            cookie_dict['path'] = sheet.cell_value(row_num, 2)
            cookie_dict['domain'] = sheet.cell_value(row_num, 3)
            cookie_dict['secure'] = bool(sheet.cell_value(row_num, 4))  # 注意因为这里的值是TRUE、FALSE是布尔类型,需要强转一下,否则会当成字符串
            cookie_dict['httpOnly'] = bool(sheet.cell_value(row_num, 5))
            cookie_list.append(cookie_dict)  # 以字典形式装入列表,[{'name':'','value':'','path':''},{},{},{},{},{}]
        for cookie in cookie_list:
            driver.add_cookie(cookie)  # 再用for循环输出并add_cookie
        driver.refresh()  # 刷新浏览器即可免密登录
    yield driver
    driver.quit()


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """失败用例截图"""
    outcome = yield
    rep = outcome.get_result()
    if rep.when == "call" and rep.failed:
        if hasattr(driver, "get_screenshot_as_png"):
            allure.attach(driver.get_screenshot_as_png(), "异常截图", allure.attachment_type.PNG)

# @pytest.mark.yaml_case('signin.yaml')
# def test_keyword():
#     pass


# 生成日志
log = Log()
log.info('步骤信息')
log.debug('调试信息')
log.warning('警告信息，一般可以继续执行')
log.error('出错信息')

