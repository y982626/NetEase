'''
coding:utf-8
@File name:operation
@Time:2023/2/23 20:59
@Author:阿波
@comment:
'''
import time

from common.Base import Base, open_browser
from page.page_positioning.positioning import *


class Store_Management(Base):
    """封装操作层,每一个元素的操作,都写成一个方法"""
    def Username(self):
        self.in_iframe('g_iframe')
        return (self.get_element_text(username))  # 获取用户名

    def Discover_music(self):
        self.out_iframe()
        return self.get_element_text(discover_music)  # 获取发现音乐

    def My_music(self):
        return self.get_element_text(my_music)  # 获取我的音乐

    def Attention(self):
        return self.get_element_text(attention)  # 获取关注

    def Mall(self):
        return self.get_element_text(mall)  # 获取商城

    def Musician(self):
        return self.get_element_text(musician)  # 音乐人

    def Download_client(self):
        return self.get_element_text(download_client)  # 下载客户端

    def Creator_center(self):
        return self.get_element_text(creator_center)  # 创作者中心

    def Search(self):
        self.send_keys(search, "一生有你")  # 搜索歌曲
if __name__ == '__main__':
    AA = Store_Management(open_browser())
    AA.no_login_token()
