'''
coding:utf-8
@File name:positioning
@Time:2023/2/23 20:58
@Author:阿波
@comment:
'''
from selenium.webdriver.common.by import By

"""封装表现层:制作每个元素的定位器"""
"""网易云首页元素"""
username = (By.ID, 'j-vip-code-to-home')  # 获取用户名
discover_music = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[1]/span/a/em')  # 获取发现音乐
my_music = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[2]/span/a/em')  # 获取我的音乐
attention = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[3]/span/a/em')  # 获取关注
mall = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[4]/span/a/em')  # 获取商城
musician = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[5]/span/a/em')  # 音乐人
download_client = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/ul/li[6]/span/a/em')  # 下载客户端
creator_center = (By.XPATH, '//*[@id="g-topbar"]/div[1]/div/a')  # 创作者中心
search = (By.XPATH, '//*[@id="srch"]')  # 搜索歌曲
