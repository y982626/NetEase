"""Men_ment的测试用例"""
from page.page_operation.operation import Store_Management


class Test_Men_ment():
    def test_username(self, open_browser_login):  # 判断用户名
        s = Store_Management(open_browser_login)
        assert s.Username() == '阿波ym'

    def test_Discover_music(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Discover_music() == '发现音乐'

    def test_Attention(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Attention() == '关注1'

    def test_mall(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Mall() == '商城'

    def test_musician(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Musician() == '音乐人'

    def test_Download_client(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Download_client() == '下载客户端'

    def test_Creator_center(self, open_browser_login):
        s = Store_Management(open_browser_login)
        assert s.Creator_center() == '创作者中心'


