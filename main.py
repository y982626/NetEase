"""
Code description: 运行测试用例
"""
import pytest
import os
import shutil
import getpathinfo
"""可以将run.py更改为man.py"""

if __name__ == '__main__':
    path = getpathinfo.get_path()  # 获取本地路径
    report_path = os.path.join(path, './logs/report—json')
    # 判断report文件夹是否存在，不存在就自动创建一个
    if not os.path.exists(report_path):
        os.mkdir(report_path)
    try:
        # 删除之前的文件夹
        shutil.rmtree("./logs/report—json")
        print('清除之前报告')
    except:
        pass
    pytest.main()

    # 直接生成报告html文件
    os.system('allure generate ./logs/report—json  -o ./allure-report/html --clean')



